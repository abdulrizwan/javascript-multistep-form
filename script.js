const page2 = document.querySelector('.content-container2');
const page3 = document.querySelector('.content-container3');
const page4 = document.querySelector('.content-container4');
const page1 = document.querySelector('.content-container1');



let page1NextButton = document.getElementById('page1-btn');

let page2NextButton = document.getElementById('page2-btn');

let page3NextButton = document.getElementById('page3-btn');

let page2Goback = document.getElementById('page2-goback');

let page3Goback = document.getElementById('page3-goback');

let arcadeDiv = document.getElementById('arcade-div');
let advancedDiv = document.getElementById('advanced-div');
let proDiv = document.getElementById('pro-div');
let finishUpHeader = document.getElementById('finish-up-header');

let arcadeDivSelector = false;
let advancedDivSelector = false;
let proDivSelector = false;

let totalSum = document.getElementById('total-sum');
let totalPrice = 0;

arcadeDiv.onclick = () => {
    if(!arcadeDivSelector){
        arcadeDiv.style.backgroundColor = 'lightgreen';
        arcadeDiv.style.border = '2px solid blue'
        proDiv.style.border = '1px solid rgb(210, 130, 239)'
        advancedDiv.style.border = '1px solid rgb(210, 130, 239)'
        advancedDiv.style.backgroundColor = 'transparent';
        proDiv.style.backgroundColor = 'transparent';
        finishUpHeader.textContent = 'Arcade';
        arcadeDivSelector = true;
        advancedDivSelector = false;
        proDivSelector = false;
    }
    else{
        arcadeDiv.style.backgroundColor = 'transparent';
        
        arcadeDivSelector = false;
    }
}


advancedDiv.onclick = () => {
    if(!advancedDivSelector){
        advancedDiv.style.backgroundColor = 'lightgreen';
        advancedDiv.style.border = '2px solid blue'
        proDiv.style.border = '1px solid rgb(210, 130, 239)'
        arcadeDiv.style.border = '1px solid rgb(210, 130, 239)'
        arcadeDiv.style.backgroundColor = 'transparent';
        proDiv.style.backgroundColor = 'transparent';
        finishUpHeader.textContent = 'Advanced';
        advancedDivSelector = true;
        arcadeDivSelector = false;
        proDivSelector = false;
    }
    else{
        advancedDiv.style.backgroundColor = 'transparent';
        advancedDivSelector = false;
    }
}

proDiv.onclick = () => {
    if(!proDivSelector){
        proDiv.style.backgroundColor = 'lightgreen';
        proDiv.style.border = '2px solid blue'
        advancedDiv.style.border = '1px solid rgb(210, 130, 239)'
        arcadeDiv.style.border = '1px solid rgb(210, 130, 239)'
        arcadeDiv.style.backgroundColor = 'transparent';
        advancedDiv.style.backgroundColor = 'transparent';
        finishUpHeader.textContent = 'Pro';
        proDivSelector = true;
        advancedDivSelector = false;
        arcadeDivSelector = false;
    }
    else{
        proDiv.style.backgroundColor = 'transparent';
        proDivSelector = false;
    }
}

let page4Goback = document.getElementById('page4-goback');

let page2ToggleButton = document.getElementById('toggleButton');

let arcadePrice = document.getElementById('arcade-price');
let advancedPrice = document.getElementById('advanced-price');
let proPrice = document.getElementById('pro-price');

let arcadeHeader = document.getElementById('arcade-header');

const number1Color = document.querySelector('.number1');
const number2Color = document.querySelector('.number2');
const number3Color = document.querySelector('.number3');
const number4Color = document.querySelector('.number4');

let yearlyText1 = document.getElementById('yearly-text1');
let yearlyText2 = document.getElementById('yearly-text2');
let yearlyText3 = document.getElementById('yearly-text3');

let lsPrice = document.getElementById('ls-price');
let osPrice = document.getElementById('os-price');
let cpPrice = document.getElementById('cp-price');

let lsFinPrice = document.getElementById('lsf-price');
let osFinPrice = document.getElementById('osf-price');
let cpFinPrice = document.getElementById('cpf-price');
let arcadeFinPrice = document.getElementById('arcade-price-fin');

let osDiv = document.getElementById('os-div');
let lsDiv = document.getElementById('ls-div');
let cpDiv = document.getElementById('cp-div');

let osCheckbox = document.getElementById('os-checkbox');
let lsCheckbox = document.getElementById('ls-checkbox');
let cpCheckbox = document.getElementById('cp-checkbox');

let osDisplay = document.getElementById('os-display');
let lsDisplay = document.getElementById('ls-display');
let cpDisplay = document.getElementById('os-display');


function validateContainer1() {
    const nameInput = document.querySelector('#name');
    const emailInput = document.querySelector('#email');
    const phoneInput = document.querySelector('#phone-number');
    const nameValidator = document.getElementById('name-validator');
    const emailValidator = document.getElementById('email-validator');
    const phoneValidator = document.getElementById('phone-validator');
    
    let isValid = true;

    nameValidator.textContent = '';
    emailValidator.textContent = '';
    phoneValidator.textContent = '';

    console.log(nameInput.value);
    if (nameInput.value.trim() === '') {
      nameValidator.textContent = 'Required*';
      isValid = false;
    }
    console.log(isValid);
    

    if (emailInput.value.trim() === '') {
        emailValidator.textContent = 'Required*';
        isValid = false;
    }

    if (phoneInput.value.trim() === '') {
      phoneValidator.textContent = 'Required*';
      isValid = false;
    }

    return isValid;
  }


page1NextButton.addEventListener('click', (event) => {
    if (!validateContainer1()) {
        event.preventDefault(); 
        return;
    }
    page2.style.display = 'block';
    page1.style.display = 'none';
    number1Color.style.backgroundColor = 'transparent';
    number2Color.style.backgroundColor = 'aqua';
})

page2Goback.addEventListener('click', () => {
    page2.style.display = 'none';
    page1.style.display = 'block';
    number2Color.style.backgroundColor = 'transparent';
    number1Color.style.backgroundColor = 'aqua';
})


let monthly = true;

page2ToggleButton.addEventListener('click', () => {
    if (monthly) {
        monthly = false;
        arcadePrice.innerText = "90";
        advancedPrice.innerText = "120";
        proPrice.innerText = "150";
        yearlyText1.style.display = 'block';
        yearlyText2.style.display = 'block';
        yearlyText3.style.display = 'block';
        osPrice.textContent = "+$10/yr";
        lsPrice.textContent = "+$20/yr";
        cpPrice.textContent = "+$20/yr";
        lsFinPrice.textContent = "+$20/yr";
        osFinPrice.textContent = "+$10/yr";
        cpFinPrice.textContent = "+$20/yr";
        arcadeHeader.textContent = "Yearly";
    }
    else {
        monthly = true;
        arcadePrice.innerText = "9";
        advancedPrice.innerText = "12";
        proPrice.innerText = "15";
        yearlyText1.style.display = 'none';
        yearlyText2.style.display = 'none';
        yearlyText3.style.display = 'none';
        osPrice.textContent = "+$1/mo";
        lsPrice.textContent = "+$2/mo";
        cpPrice.textContent = "+$2/mo";
        arcadeHeader.textContent = "Monthly";
        lsFinPrice.textContent = "+$2/mo";
        osFinPrice.textContent = "+$1/mo";
        cpFinPrice.textContent = "+$2/mo";
    }
})

page2NextButton.addEventListener('click', () => {
    if(!advancedDivSelector && !arcadeDivSelector && !proDivSelector){
        alert('Select on choice');
    }
    page2.style.display = 'none';
    page1.style.display = 'none';
    page3.style.display = 'block';
    number1Color.style.backgroundColor = 'transparent';
    number2Color.style.backgroundColor = 'transparent';
    number3Color.style.backgroundColor = 'aqua';
    if(monthly && arcadeDivSelector){
        arcadeFinPrice.textContent = '$9/mo';
        totalPrice = 9;
        if(osCheckbox.checked){
            totalPrice += 1;
        }
        if(lsCheckbox.checked){
            totalPrice += 2;
        }
        if(cpCheckbox.checked){
            totalPrice += 2;
        }
    }
    if(monthly && advancedDivSelector){
        arcadeFinPrice.textContent = '$12/mo';
        totalPrice = 12;
        if(osCheckbox.checked){
            totalPrice += 1;
        }
        if(lsCheckbox.checked){
            totalPrice += 2;
        }
        if(cpCheckbox.checked){
            totalPrice += 2;
        }
    }
    if(monthly && proDivSelector){
        arcadeFinPrice.textContent = '$15/mo';
        totalPrice = 15;
        if(osCheckbox.checked){
            totalPrice += 1;
        }
        if(lsCheckbox.checked){
            totalPrice += 2;
        }
        if(cpCheckbox.checked){
            totalPrice += 2;
        }
    }
    if(!monthly && arcadeDivSelector){
        arcadeFinPrice.textContent = '$90/yr';
        totalPrice = 90;
        if(osCheckbox.checked){
            totalPrice += 10;
        }
        if(lsCheckbox.checked){
            totalPrice += 20;
        }
        if(cpCheckbox.checked){
            totalPrice += 20;
        }
    }
    if(!monthly && advancedDivSelector){
        arcadeFinPrice.textContent = '$120/yr';
        totalPrice = 120;
        if(osCheckbox.checked){
            totalPrice += 10;
        }
        if(lsCheckbox.checked){
            totalPrice += 20;
        }
        if(cpCheckbox.checked){
            totalPrice += 20;
        }
    }
    if(!monthly && proDivSelector){
        arcadeFinPrice.textContent = '$150/yr';
        totalPrice = 150;
        if(osCheckbox.checked){
            totalPrice += 10;
        }
        if(lsCheckbox.checked){
            totalPrice += 20;
        }
        if(cpCheckbox.checked){
            totalPrice += 20;
        }
    }
})

page3Goback.addEventListener('click', () => {
    page2.style.display = 'block';
    page1.style.display = 'none';
    page3.style.display = 'none';
    number1Color.style.backgroundColor = 'transparent';
    number2Color.style.backgroundColor = 'aqua';
    number3Color.style.backgroundColor = 'transparent';

})

osCheckbox.addEventListener('click', () => {
    if(osCheckbox.checked){
        document.querySelector('.finish-up-inner-container-os').style.display = 'flex';
        if(monthly){
            totalPrice += 1;
        }
        else{
            totalPrice += 10
        }
    }
    else{
        document.querySelector('.finish-up-inner-container-os').style.display = 'none';
        if(monthly){
            totalPrice -= 1;
        }
        else{
            totalPrice -= 10
        }
    }
})

lsCheckbox.addEventListener('click', () => {
    if(lsCheckbox.checked){
        document.querySelector('.finish-up-inner-container-ls').style.display = 'flex';
        if(monthly){
            totalPrice += 2;
        }
        else{
            totalPrice += 20;
        }
    }
    else{
        document.querySelector('.finish-up-inner-container-ls').style.display = 'none';
        if(monthly){
            totalPrice -= 2;
        }
        else{
            totalPrice -= 20;
        }
    }
})

cpCheckbox.addEventListener('click', () => {
    if(cpCheckbox.checked){
        document.querySelector('.finish-up-inner-container-cp').style.display = 'flex';
        if(monthly){
            totalPrice += 2;
        }
        else{
            totalPrice += 20;
        }
    }
    else{
        document.querySelector('.finish-up-inner-container-cp').style.display = 'none';
        if(monthly){
            totalPrice -= 2;
        }
        else{
            totalPrice -= 20;
        }
    }
})


page3NextButton.addEventListener('click', () => {
    page4.style.display = 'block';
    page1.style.display = 'none';
    page3.style.display = 'none';
    page2.style.display = 'none';
    number1Color.style.backgroundColor = 'transparent';
    number2Color.style.backgroundColor = 'transparent';
    number3Color.style.backgroundColor = 'transparent';
    number4Color.style.backgroundColor = 'aqua';
    if(monthly){
        totalSum.textContent = `$${totalPrice}/mo`;
    }
    else{
        totalSum.textContent = `$${totalPrice}/yr`;
    }
    
})

page4Goback.addEventListener('click', () => {
    page4.style.display = 'none';
    page1.style.display = 'none';
    page3.style.display = 'block';
    page2.style.display = 'none';
    number1Color.style.backgroundColor = 'transparent';
    number2Color.style.backgroundColor = 'transparent';
    number3Color.style.backgroundColor = 'aqua';
    number4Color.style.backgroundColor = 'transparent';
})

const confirmButton = document.getElementById('confirm-btn');

let page5 = document.querySelector('.info-content');
confirmButton.addEventListener('click', function () {
    page4.style.display = 'none';
    page1.style.display = 'none';
    page3.style.display = 'none';
    page2.style.display = 'none';
    page5.style.display = 'block';
})

